<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Note that I have not used  middleware since it's not mentioned in the task

//api to purchase dish
Route::post('/postDishOrder', [\App\Http\Controllers\DishController::class, 'postDishOrder']);

//api to show aggregated data
Route::get('/getAggregatedData', [\App\Http\Controllers\DishController::class, 'getAggregatedData']);

//api to show max and min data
Route::get('/getMaxMinData', [\App\Http\Controllers\DishController::class, 'getMaxMinData']);
