<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Jobs\SendEmailJob;
use App\Models\Dish;
use App\Models\DishPurchased;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Request;
use Response;

class DishController extends Controller
{

    public function postDishOrder(ProductRequest $request) : JsonResponse{
        try {
            $dishId = $request->input("dishId");
            $qty = $request->input("qty");
            $dish = Dish::whereId($dishId)->first();
            if (!$dish){
                return response()->json([
                    'message' => 'Purchase UnSuccessfull! There is no such dish',
                    'status' => false,
                ]);
            }
            if ($dish->availablity < $qty){
                $details['email'] = ' contact@subdine.com';
                $details['product'] = $dish->name;
                dispatch(new SendEmailJob($details));
                return response()->json([
                    'message' => 'Purchase UnSuccessfull',
                    'status' => false,
                ]);
            }
            else{
                $qtyLeft = $dish->availablity - $qty;
                DB::beginTransaction();
                Dish::whereId($dishId)->update(['availablity' => $qtyLeft]);
                $dishPurchased = new DishPurchased();
                $dishPurchased->dish_id = $dishId;
                $dishPurchased->qty = $qty;
                $dishPurchased->save();
                DB::commit();
                return response()->json([
                    'message' => 'Purchase Successfull',
                    'status' => true,
                ]);
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => 'Something Went Wrong! '.$e->getMessage(),
                'status' => false,
            ]);
        }
    }

    public function getAggregatedData(){
        $twoDaysBeforeDate = date('Y-m-d', strtotime('-2 days'));
        $dishes = DishPurchased::select(DB::raw("SUM(qty) as totalQty"),'dishes.name','dish_purchases.dish_id')->where('dish_purchases.created_at', '>',$twoDaysBeforeDate)->join('dishes','dishes.id','=','dish_purchases.dish_id')->groupBy('dish_purchases.dish_id')->groupBy('dishes.name')->get();

        return response()->json([
            'message' => 'Api Success!' ,
            'status' => true,
            'data' => $dishes
        ]);
    }

    public function getMaxMinData(){
        $tenDaysBeforeDate = date('Y-m-d', strtotime('-10 days'));
        $MostSoldDishes = DishPurchased::select(DB::raw("SUM(qty) as totalQty"),'dishes.name')->where('dish_purchases.created_at', '>',$tenDaysBeforeDate)->join('dishes','dishes.id','=','dish_purchases.dish_id')->groupBy('dish_purchases.dish_id')->groupBy('dishes.name')->orderBy('totalQty', 'desc')->limit(5)->get();

        $LeastSoldDishes = DishPurchased::select(DB::raw("SUM(qty) as totalQty"),'dishes.name')->where('dish_purchases.created_at', '>',$tenDaysBeforeDate)->join('dishes','dishes.id','=','dish_purchases.dish_id')->groupBy('dish_purchases.dish_id')->groupBy('dishes.name')->orderBy('totalQty', 'asc')->limit(5)->get();

        $data = array();
        $data['5MostSold'] = $MostSoldDishes;
        $data['5LeastSold'] = $LeastSoldDishes;

        return response()->json([
            'message' => 'Api Success!' ,
            'status' => true,
            'data' => $data
        ]);
    }

}
