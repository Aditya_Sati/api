<?php


namespace App\Http\Requests;


use App\UserRoles;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dishId' => 'required',
            'qty' => 'required'
        ];
    }

    public function message()
    {
        return [
            'dishId' => 'Dish Id is required.',
            'qty' => 'Qty is required.'
        ];
    }
}
