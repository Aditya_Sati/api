<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DishPurchased extends Model
{
    protected $table = "dish_purchases";
    protected $guarded = ['id'];
}
